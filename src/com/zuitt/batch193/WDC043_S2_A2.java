package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class WDC043_S2_A2 {

    public static void main(String[] args){

        Scanner inputScanner = new Scanner(System.in);
        System.out.println("Input an index (0-4)");

        int inputIndex = inputScanner.nextInt();

        int[] intPrime = new int[5];

        intPrime[0] = 2;
        intPrime[1] = 3;
        intPrime[2] = 5;
        intPrime[3] = 7;
        intPrime[4] = 11;
        System.out.println("The prime number at index " + inputIndex + " is " + intPrime[inputIndex]);

        ArrayList<String> friends = new ArrayList<>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My Friends are: " + friends);


        HashMap<String, Integer> products = new HashMap<>();

        products.put("Toothpaste", 25);
        products.put("Toothbrush", 15);
        products.put("Soap", 35);
        System.out.println("Our current inventory consists of: " + products);


    }

}
